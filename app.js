function showTime() {
    var now = new Date();

    hours = now.getHours();
    minutes = now.getMinutes();
    seconds = now.getSeconds();

    hours = hours < 10 ? '0' + hours : hours;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;
    var timeString = hours + ":" + minutes + ":" + seconds;

    document.getElementById('hours').innerHTML = hours;
    document.getElementById('minutes').innerHTML = ":" + minutes;
    document.getElementById('seconds').innerHTML = "." + seconds;
}

showTime();
window.addEventListener("load", function() {
    timer = setInterval(showTime, 1000);
});
